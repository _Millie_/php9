-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Apr 06, 2018 at 10:19 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movies`
--

-- --------------------------------------------------------

--
-- Table structure for table `film`
--

DROP TABLE IF EXISTS `film`;
CREATE TABLE IF NOT EXISTS `film` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_name` varchar(100) CHARACTER SET utf8 NOT NULL,
  `id_genre` int(11) NOT NULL,
  `runnnig_time` varchar(50) CHARACTER SET utf8 NOT NULL,
  `description` varchar(200) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_genre` (`id_genre`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `film`
--

INSERT INTO `film` (`id`, `movie_name`, `id_genre`, `runnnig_time`, `description`) VALUES
(1, 'Spectre', 1, '188 minutes', 'A cryptic message from Bond\'s past, sends him on a trail to uncover a sinister organization. While M battles political forces to keep the Secret '),
(2, 'The ftyytf', 2, '100 minutes', 'Three buddies wake up from a bachelor party in Las Vegas, with no memory of the previous night and the bachelor missing.'),
(3, 'Saw', 5, '100 minutes', 'Two strangers, who awaken in a room with no recollection of how they got there, soon discover they\'re pawns in a deadly game perpetrated by a notorious serial killer.'),
(4, 'GHABUABHU', 5, 'fsgfdg', 'gdrg'),
(5, 'Thor', 3, '130 minutes', ' The powerful, but arrogant god Thor, is cast out of Asgard to live amongst humans in Midgard (Earth), where he soon becomes one of their finest defenders. ');

-- --------------------------------------------------------

--
-- Table structure for table `genre`
--

DROP TABLE IF EXISTS `genre`;
CREATE TABLE IF NOT EXISTS `genre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `genre_name` varchar(100) NOT NULL,
  `description_genre` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `genre`
--

INSERT INTO `genre` (`id`, `genre_name`, `description_genre`) VALUES
(1, 'Action', 'Action film is a genre in which the protagonist or protagonists are thrust into a series of challenges that typically include violence, extended fighting, physical feats, and frantic chases.'),
(2, 'Comedy', 'is a genre with light-hearted, humorous plotlines, centered on romantic ideals such as that true love is able to surmount most obstacles'),
(3, 'Animated movie', 'An animated cartoon is a film for the cinema, television or computer screen, which is made using sequential drawings, as opposed to animations in general.'),
(4, 'Adventure movies', 'Adventure films are a genre of film that typically use their action scenes to display and explore exotic locations in an energetic way'),
(5, 'Horror', 'A horror film is a movie that seeks to elicit a physiological reaction, such as an elevated heartbeat, through the use of fear and shocking one’s audiences.');

-- --------------------------------------------------------

--
-- Table structure for table `schedule`
--

DROP TABLE IF EXISTS `schedule`;
CREATE TABLE IF NOT EXISTS `schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_filma` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `location` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `id_film` (`id_filma`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `schedule`
--

INSERT INTO `schedule` (`id`, `id_filma`, `date`, `time`, `location`) VALUES
(1, 1, '2018-04-10', '19:19:27', 'Lifka'),
(2, 2, '2018-04-27', '20:00:00', 'Radnicki'),
(3, 4, '2018-04-30', '17:00:00', 'Lifka'),
(4, 3, '2018-04-28', '21:00:00', 'Lifka'),
(5, 5, '2018-04-24', '20:00:00', 'Lifka');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `film`
--
ALTER TABLE `film`
  ADD CONSTRAINT `id_genre` FOREIGN KEY (`id_genre`) REFERENCES `genre` (`id`);

--
-- Constraints for table `schedule`
--
ALTER TABLE `schedule`
  ADD CONSTRAINT `id_film` FOREIGN KEY (`id_filma`) REFERENCES `film` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
